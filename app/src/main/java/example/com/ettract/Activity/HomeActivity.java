package example.com.ettract.Activity;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;

import example.com.ettract.Adapter.Pager;
import example.com.ettract.R;

import static android.support.v7.appcompat.R.id.wrap_content;

/**
 * Created by incipientinfo_pc4 on 27/12/16.
 */

public class HomeActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, ViewPager.OnPageChangeListener {

    private TabLayout tabLayout;

    private ViewPager viewPager;

    private static Boolean isLoggedIn;

    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homescreen);

        context = this;

        init();

        sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        editor = sharedPreferences.edit();

        setListeners();

        if (getIntent().hasExtra("login")) {
            isLoggedIn = false;
            editor.putBoolean("isLoggedIn", isLoggedIn);
            editor.commit();
        } else {
            isLoggedIn = true;
            editor.putBoolean("isLoggedIn", isLoggedIn);
            editor.commit();
        }

        viewPager.setCurrentItem(1);

    }

    private void init() {

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        tabLayout.addTab(tabLayout.newTab().setText("Map"));
        tabLayout.addTab(tabLayout.newTab().setText("List"));

        LinearLayout linearLayout = (LinearLayout) tabLayout.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.parseColor("#e0e0e0"));
        drawable.setSize(2, wrap_content);
        linearLayout.setDividerDrawable(drawable);

        viewPager = (ViewPager) findViewById(R.id.pager);

        Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

    }

    private void setListeners() {

        tabLayout.setOnTabSelectedListener(this);
        viewPager.addOnPageChangeListener(this);

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }


    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {

        viewPager.setCurrentItem(position, false);
        tabLayout.getTabAt(position).select();
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}