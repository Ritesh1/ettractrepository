package example.com.ettract.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Timer;
import java.util.TimerTask;

import example.com.ettract.R;
import example.com.ettract.Utility.Utility;

/**
 * Created by incipientinfo_pc4 on 27/12/16.
 */

public class DetailListActivity extends AppCompatActivity implements View.OnClickListener {

    String json_array;

    private ViewPager viewPager;

    JSONArray img_array;

    String user_id, title, time, location, profile, user_name, desc, tv_category;

    TextView tv_location, tv_time, tv_title, tv_user, tv_desc;

    ImageView img_profile;

    ImageView img_category;

    Context context;

    private static int currentPage = 0;

    private static int NUM_PAGES = 0;

    TextView toolbar_txt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.detail_row_list);

        context = this;

        findViewById();

        try {

            img_array = new JSONArray(json_array);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        viewPager = (ViewPager) findViewById(R.id.tutorial_pager);
        viewPager.setAdapter(new ViewPagerAdapter(DetailListActivity.this));


        NUM_PAGES = img_array.length();

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 1500, 1500);

        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.tutorial_pager_indicator);
        indicator.setSnap(true);
        indicator.setViewPager(viewPager);
        indicator.setRadius(Utility.getPxFromDp(this, 5.83f));
        indicator.setFillColor(0xffcddc39);
        indicator.setPageColor(0x33666666);
        indicator.setStrokeWidth(0);

        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });


    }

    private void findViewById() {

        user_id = getIntent().getStringExtra("user_id");

        json_array = getIntent().getStringExtra("img_list_array");

        title = getIntent().getStringExtra("title");

        time = getIntent().getStringExtra("time");

        location = getIntent().getStringExtra("location");

        profile = getIntent().getStringExtra("profile_img");

        user_name = getIntent().getStringExtra("user_name");

        desc = getIntent().getStringExtra("event_description");

        tv_category = getIntent().getStringExtra("category");

        tv_location = (TextView) findViewById(R.id.tv_location);

        tv_desc = (TextView) findViewById(R.id.tv_desc);

        tv_time = (TextView) findViewById(R.id.tv_time);

        tv_title = (TextView) findViewById(R.id.tv_title);

        tv_user = (TextView) findViewById(R.id.tv_user);

        img_profile = (ImageView) findViewById(R.id.img_profile);

        img_category = (ImageView) findViewById(R.id.img_category);

        toolbar_txt = (TextView) findViewById(R.id.toolbar_txt);

        tv_user.setText(user_name);

        tv_title.setText(title);

        tv_time.setText(time);

        tv_desc.setText(desc);

        tv_location.setText(location);

        Picasso.with(context)
                .load(profile)
                .into(img_profile);


        if (tv_category.equals("1"))

        {
            img_category.setImageDrawable(getResources().getDrawable(R.drawable.strong));
        }

        if (tv_category.equals("2"))

        {

            img_category.setImageDrawable(getResources().getDrawable(R.drawable.game));

        }

        if (tv_category.equals("3"))

        {
            img_category.setImageDrawable(getResources().getDrawable(R.drawable.face));

        }
        if (tv_category.equals("4"))

        {
            img_category.setImageDrawable(getResources().getDrawable(R.drawable.deal));

        }
        if (tv_category.equals("5"))

        {
            img_category.setImageDrawable(getResources().getDrawable(R.drawable.unspecified));

        }

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

        }
    }


    private class ViewPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;

        public ViewPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return img_array.length();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.row_view_pager_item2, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
            try {


                String url = img_array.getJSONObject(position).getString("link");


                Picasso.with(mContext)
                        .load(url)
                        .into(imageView);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            container.addView(itemView);

            return itemView;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }

}
