package example.com.ettract.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import example.com.ettract.R;
import example.com.ettract.Utility.Utility;

/**
 * Created by incipientinfo_pc4 on 27/12/16.
 */

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    TextView btn_signup;

    TextView tv_login;

    ProgressDialog dialog;

    Context context;

    EditText edit_phone, edit_password;

    TextView txt_skip_login;

    CoordinatorLayout coordinateLayout;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_signup);

        context = this;

        findViewById();

        sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);

        setOnListners();

    }

    private void setOnListners() {

        btn_signup.setOnClickListener(this);

        tv_login.setOnClickListener(this);

        txt_skip_login.setOnClickListener(this);
    }

    private void findViewById() {

        btn_signup = (TextView) findViewById(R.id.btn_signup);

        tv_login = (TextView) findViewById(R.id.tv_login);

        edit_phone = (EditText) findViewById(R.id.edit_phone);

        edit_password = (EditText) findViewById(R.id.edit_password);

        txt_skip_login = (TextView) findViewById(R.id.txt_skip_login);

        coordinateLayout = (CoordinatorLayout) findViewById(R.id.coordinateLayout);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_signup:

                if (edit_phone.getText().toString().equals("") && edit_phone.getText().toString().isEmpty()) {

                    Utility.showSnackBarForCoordinateLayout(coordinateLayout, "please enter phone number");

                } else if (edit_password.getText().toString().equals("") && edit_password.getText().toString().isEmpty()) {

                    Utility.showSnackBarForCoordinateLayout(coordinateLayout, "please enter password");

                } else {

                    if (Utility.isInternetConnectionAvailable(context)) {

                        signUp();

                    } else {

                        Utility.showSnackBarForCoordinateLayout(coordinateLayout, "No Internet Connection");
                    }
                }

                break;

            case R.id.tv_login:

                Intent i2 = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(i2);
                finish();

                break;

            case R.id.txt_skip_login:

                Intent i = new Intent(SignUpActivity.this, HomeActivity.class);
                i.putExtra("login", "skipped");

                startActivity(i);
                finish();

                break;
        }

    }


    private void signUp() {

        callProgressDialog();

        String url = getResources().getString(R.string.server_url_signup);

        String data = "{\"data\":[{\"phoneno\":\"" + edit_phone.getText().toString() + "\",\"password\":\"" + edit_password.getText().toString() + "\",\"token_no\":\"" + sharedPreferences.getString("token_no", "") + "\"}]}";
        JSONObject jsonObjectForrequest = null;

        try {

            jsonObjectForrequest = new JSONObject(data);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = null;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObjectForrequest, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                dialog.dismiss();


                if (response.has("success"))

                {
                    saveDataToSharedPreference(response);

                    Intent i = new Intent(SignUpActivity.this, HomeActivity.class);

                    startActivity(i);

                    finish();

                } else {

                    try {

                        int error_code = response.getJSONObject("error").getInt("code");

                        String msg = response.getJSONObject("error").getString("msg");

                        if (error_code == 0)

                        {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }

                        if (error_code == 1)

                        {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }

                        if (error_code == 2)

                        {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }

                        if (error_code == 3)

                        {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }


                }
            }
        }
                , new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        jsonObjectRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        requestQueue.add(jsonObjectRequest);

    }


    private void saveDataToSharedPreference(JSONObject response) {

        SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        try {

            editor.putString("login_", "logged_in");
            editor.putBoolean("login", true);
            editor.putString("id", response.getJSONObject("success").getString("id"));
            editor.putString("phoneno", response.getJSONObject("success").getString("phoneno"));
            editor.putBoolean("login", true);
            editor.commit();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void callProgressDialog() {
        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Loading..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


}