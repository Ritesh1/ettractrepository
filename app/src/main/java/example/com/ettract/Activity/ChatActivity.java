package example.com.ettract.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import example.com.ettract.Adapter.ChatHistoryAdapter;
import example.com.ettract.Modal.ChatHistoryModel;
import example.com.ettract.R;
import example.com.ettract.Utility.Sqlite;

/**
 * Created by incipientinfo_pc4 on 27/12/16.
 */

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;

    TextView toolbar_txt;

    LinearLayout long_press;

    ImageView img_delete, img_warnning;

    Context context;

    ListView listView;

    Sqlite sqlite;

    public static ArrayList<ChatHistoryModel> chatHistoryModelArrayList;

    public static ChatHistoryAdapter chatHistryAdapter;

    ImageView iv_no_data;

    public static boolean main_active = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        context = this;

        findViewById();

        setListners();

        toolbar.setTitle("");
        toolbar_txt.setText("Messages");

        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(ChatActivity.this, HomeActivity.class);
                startActivity(i);
            }
        });

        sqlite = new Sqlite(context);

        String url = "SELECT receiver_id, name, msg FROM tbl_chat GROUP BY receiver_id ORDER BY datetime DESC";

        chatHistoryModelArrayList = sqlite.fetchChat(url);

        listView = (ListView) findViewById(R.id.listview_chat);

        iv_no_data = (ImageView) findViewById(R.id.iv_no_data);

        if (chatHistoryModelArrayList.size() > 0) {
            chatHistryAdapter = new ChatHistoryAdapter(context, chatHistoryModelArrayList);
        } else {
            iv_no_data.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }


        listView.setAdapter(chatHistryAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent(context, ChatActivityNew.class);
                intent.putExtra("user_id", chatHistoryModelArrayList.get(i).getReceiver_id());
                intent.putExtra("user_name", chatHistoryModelArrayList.get(i).getName());
                startActivity(intent);

            }
        });
    }

    private void findViewById() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar_txt = (TextView) findViewById(R.id.toolbar_txt);

        /*long_press = (LinearLayout) findViewById(R.id.long_press);

        img_delete = (ImageView) findViewById(R.id.img_delete);
        img_warnning = (ImageView) findViewById(R.id.img_warnning);

        long_press.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // TODO Auto-generated method stub

                Toast.makeText(context, "long clicked..", Toast.LENGTH_SHORT).show();

                return true;
            }
        });*/

    }

    private void setListners() {

        //long_press.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

           /* case R.id.long_press:

                Intent i2 = new Intent(ChatActivity.this, ReportChatDetailActivity.class);
                startActivity(i2);
                finish();

                break;*/


        }
    }

    @Override
    public void onStart() {
        super.onStart();
        main_active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        main_active = false;
    }
}
