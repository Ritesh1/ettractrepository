package example.com.ettract.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import example.com.ettract.R;
import example.com.ettract.Utility.CircleTransformPicasso;


/**
 * Created by incipientinfo_pc4 on 20/12/16.
 */

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edit_user_name, edit_city;

    ImageView img_camera, img_profile, img_save, img_pencil;

    Context context;

    Bitmap bm;

    ProgressDialog dialog;

    Toolbar toolbar;

    TextView toolbar_txt;

    public final static int REQUEST_CODE_FOR_GALLARY = 1;

    public final static int REQUEST_CODE_FOR_SELECT_FILE = 2;

    public final static int CAMERA_REQUEST = 3;

    LinearLayout removeaccount;

    String user_id;
    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    LinearLayout logout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);

        context = this;

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        editor = sharedPreferences.edit();

        user_id = sharedPreferences.getString("id", "");

        getProfile();

        findViewById();

        setListners();
    }

    private void setListners() {

        img_save.setOnClickListener(this);

        img_camera.setOnClickListener(this);

        removeaccount.setOnClickListener(this);

        logout.setOnClickListener(this);

    }

    private void findViewById() {

        edit_user_name = (EditText) findViewById(R.id.edit_user_name);

        edit_city = (EditText) findViewById(R.id.edit_city);

        img_camera = (ImageView) findViewById(R.id.img_camera);

        img_profile = (ImageView) findViewById(R.id.img_profile);

        img_save = (ImageView) findViewById(R.id.img_save);

        img_pencil = (ImageView) findViewById(R.id.img_pencil);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar_txt = (TextView) findViewById(R.id.toolbar_txt);

        toolbar.setTitle("");
        toolbar_txt.setText("Edit Profile");

        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(EditProfileActivity.this, ProfileActivity.class);
                startActivity(i);
                finish();
            }
        });



        img_pencil.setVisibility(View.GONE);

        img_save.setVisibility(View.VISIBLE);

        removeaccount = (LinearLayout) findViewById(R.id.removeaccount);

        logout = (LinearLayout) findViewById(R.id.logout);

    }


    private void getProfile() {

        callProgressDialog();

        String url = getResources().getString(R.string.server_url_get_profile);

        String data = "{\"data\":[{\"user_id\":\"" + user_id + "\"}]}";
        JSONObject jsonObjectForrequest = null;

        try {

            jsonObjectForrequest = new JSONObject(data);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = null;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObjectForrequest, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                dialog.dismiss();


                if (response.has("success")) {

                    try {

                        String user = response.getJSONObject("success").getString("username");

                        String city = response.getJSONObject("success").getString("city");

                        String img = response.getJSONObject("success").getString("profile");

                        if (!img.equals("")) {

                            Picasso.with(context)
                                    .load(img)
                                    .transform(new CircleTransformPicasso(context))
                                    .placeholder(R.drawable.prof_bg)
                                    .into(img_profile);
                        } else {

                            Picasso.with(context)
                                    .load(R.drawable.prof_bg)
                                    .transform(new CircleTransformPicasso(context))
                                    .into(img_profile);
                        }


                        edit_user_name.setText(user);
                        edit_user_name.setSelection(edit_user_name.getText().length());

                        edit_city.setText(city);
                        edit_city.setSelection(edit_city.getText().length());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {

                    try {

                        int error_code = response.getJSONObject("error").getInt("code");

                        String msg = response.getJSONObject("error").getString("msg");

                        if (error_code == 0) {

                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            }
        }
                , new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        jsonObjectRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        requestQueue.add(jsonObjectRequest);

    }

    private void serverCallForEditProfile() {

        callProgressDialog();

        String str_image_path = null;

        if (bm != null)

        {
            str_image_path = convertImageToString();
        }

        String url = "http://dev5.demo4app.com/ettract/jsonapi/profile";

        String data = "{\"data\":[{\"user_id\":\"" + user_id + "\",\"username\":\"" + edit_user_name.getText().toString() + "\",\"city\":\"" + edit_city.getText().toString() + "\",\"profile\":\"" + str_image_path + "\"}]}\t";
        JSONObject jsonObjectForrequest = null;

        try {

            jsonObjectForrequest = new JSONObject(data);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = null;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObjectForrequest, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                dialog.dismiss();

                if (response.has("success")) {


                    //finish();

                } else {

                    try {

                        int error_code = response.getJSONObject("error").getInt("code");

                        String msg = response.getJSONObject("error").getString("msg");

                        if (error_code == 0)

                        {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }

                        if (error_code == 1)

                        {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }

                        if (error_code == 2)

                        {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }


                }
            }
        }
                , new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        jsonObjectRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        requestQueue.add(jsonObjectRequest);

    }


    private void requestPermission() {

        String[] PERMISSIONS_CALL = {Manifest.permission.READ_EXTERNAL_STORAGE};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            requestPermissions(PERMISSIONS_CALL, REQUEST_CODE_FOR_GALLARY);
        } else

        {
            galleryIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_FOR_GALLARY:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    galleryIntent();

                }
                break;
        }
    }


    private void galleryIntent() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select File"), REQUEST_CODE_FOR_SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_FOR_SELECT_FILE)

                onSelectFromGalleryResult(data);
        }

        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            img_profile.setImageBitmap(photo);
        }
    }

    private void onSelectFromGalleryResult(Intent data) {

        String path = null;

        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());

                path = MediaStore.Images.Media.insertImage(this.getContentResolver(), bm, "any_Title", null);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        int density = getResources().getDisplayMetrics().densityDpi;

        switch (density) {

            case DisplayMetrics.DENSITY_MEDIUM:

                setImgWithPicasso(path, 100, 150);

                break;
            case DisplayMetrics.DENSITY_HIGH:

                setImgWithPicasso(path, 150, 200);

                break;
            case DisplayMetrics.DENSITY_XHIGH:

                setImgWithPicasso(path, 200, 250);

                break;
            case DisplayMetrics.DENSITY_XXHIGH:

                setImgWithPicasso(path, 250, 300);

                break;
            case DisplayMetrics.DENSITY_XXXHIGH:

                setImgWithPicasso(path, 300, 350);

                break;
        }
    }

    public void setImgWithPicasso(String path, int img_w, int img_h)

    {
        Picasso.with(this)
                .load(path)
                .resize(img_w, img_h)
                .transform(new CircleTransformPicasso(context, 1))
                .into(img_profile);
    }

    private String convertImageToString() {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public void callProgressDialog() {
        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Loading..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.img_save:

                bm = ((BitmapDrawable) img_profile.getDrawable()).getBitmap();

                serverCallForEditProfile();

                break;

            case R.id.img_camera:

                if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)

                {
                    requestPermission();
                } else

                {

                    profileDailog();

                }

                break;

            case R.id.removeaccount:

                RemoveDailog(user_id);

                break;

            case R.id.logout:

                editor.clear();
                editor.commit();

                Intent intent1 = new Intent(context, LoginActivity.class);
                startActivity(intent1);
                finish();

                break;
        }
    }

    public void RemoveDailog(final String id) {


        final Dialog dialog2 = new Dialog(context);
        dialog2.setCancelable(true);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(R.layout.dailog_remove_account);

        Button ok = (Button) dialog2.findViewById(R.id.btn_ok);

        Button cancel = (Button) dialog2.findViewById(R.id.btn_cancel);


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog2.dismiss();

                removeAccount(id);


            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog2.dismiss();
            }
        });


        dialog2.show();
    }

    private void removeAccount(String user_id) {

        callProgressDialog();

        String url = getResources().getString(R.string.server_url_remove_user);

        String data = "{\"data\":[{\"user_id\":\"" + user_id + "\"}]}";
        JSONObject jsonObjectForrequest = null;

        try {

            jsonObjectForrequest = new JSONObject(data);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = null;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObjectForrequest, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                dialog.dismiss();


                if (response.has("success"))

                {

                    Toast.makeText(context, "user seccessfully removed..", Toast.LENGTH_SHORT).show();

                    editor.clear();
                    editor.commit();

                    Intent i = new Intent(EditProfileActivity.this, SignUpActivity.class);
                    startActivity(i);
                    finish();

                } else

                {

                    try {

                        int error_code = response.getJSONObject("error").getInt("code");

                        String msg = response.getJSONObject("error").getString("msg");

                        if (error_code == 0)

                        {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();

                    }


                }
            }
        }
                , new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        jsonObjectRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        requestQueue.add(jsonObjectRequest);

    }

    public void profileDailog() {
        final Dialog dialog2 = new Dialog(context);

        dialog2.setCancelable(true);

        dialog2.setContentView(R.layout.dailog_choose_profile);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);

        TextView camera = (TextView) dialog2.findViewById(R.id.camera);

        TextView gallary = (TextView) dialog2.findViewById(R.id.gallary);

        TextView remove = (TextView) dialog2.findViewById(R.id.remove);


        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog2.dismiss();

                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);


            }
        });


        gallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog2.dismiss();

                galleryIntent();


            }
        });


        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog2.dismiss();

                img_profile.setImageResource(R.drawable.prof2);

                BitmapDrawable drawable = (BitmapDrawable) img_profile.getDrawable();
                bm = drawable.getBitmap();

            }
        });


        dialog2.show();
    }


}
