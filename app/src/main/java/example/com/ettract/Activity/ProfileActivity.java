package example.com.ettract.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import example.com.ettract.R;
import example.com.ettract.Utility.CircleTransformPicasso;


/**
 * Created by incipientinfo_pc4 on 27/12/16.
 */

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {


    ImageView img_pancil;

    Toolbar toolbar;

    TextView toolbar_txt;

    LinearLayout removeaccount;

    Context context;

    ProgressDialog dialog;

    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    String user_id;

    LinearLayout logout;

    ImageView img_profile;

    TextView user_name, txt_city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        editor = sharedPreferences.edit();

        user_id = sharedPreferences.getString("id", "");

        context = this;

        findViewById();

        getProfile();

        setListners();

    }

    private void setListners() {

        img_pancil.setOnClickListener(this);

        removeaccount.setOnClickListener(this);

        logout.setOnClickListener(this);

    }

    private void findViewById() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar_txt = (TextView) findViewById(R.id.toolbar_txt);

        toolbar.setTitle("");
        toolbar_txt.setText("Profile");

        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i2 = new Intent(ProfileActivity.this, HomeActivity.class);
                startActivity(i2);
                finish();
            }
        });

        img_pancil = (ImageView) findViewById(R.id.img_pencil);
        img_pancil.setVisibility(View.VISIBLE);

        removeaccount = (LinearLayout) findViewById(R.id.removeaccount);

        logout = (LinearLayout) findViewById(R.id.logout);

        img_profile = (ImageView) findViewById(R.id.img_profile);

        user_name = (TextView) findViewById(R.id.user_name);

        txt_city = (TextView) findViewById(R.id.txt_city);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.img_pencil:

                Intent i = new Intent(ProfileActivity.this, EditProfileActivity.class);
                startActivity(i);

                break;

            case R.id.removeaccount:

                RemoveDailog(user_id);

                break;

            case R.id.logout:

                editor.clear();
                editor.commit();

                Intent intent1 = new Intent(context, LoginActivity.class);
                startActivity(intent1);
                finish();

                break;
        }
    }

    public void RemoveDailog(final String id) {

        final Dialog dialog2 = new Dialog(context);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setCancelable(true);
        dialog2.setContentView(R.layout.dailog_remove_account);

        Button ok = (Button) dialog2.findViewById(R.id.btn_ok);

        Button cancel = (Button) dialog2.findViewById(R.id.btn_cancel);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog2.dismiss();
                removeAccount(id);
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog2.dismiss();
            }
        });

        dialog2.show();
    }


    private void removeAccount(String user_id) {

        callProgressDialog();

        String url = getResources().getString(R.string.server_url_remove_user);

        String data = "{\"data\":[{\"user_id\":\"" + user_id + "\"}]}";
        JSONObject jsonObjectForrequest = null;

        try {

            jsonObjectForrequest = new JSONObject(data);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = null;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObjectForrequest, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                dialog.dismiss();

                if (response.has("success"))

                {
                    editor.clear();
                    editor.commit();

                    Intent i = new Intent(ProfileActivity.this, SignUpActivity.class);
                    startActivity(i);
                    finish();

                } else {
                    try {

                        int error_code = response.getJSONObject("error").getInt("code");

                        String msg = response.getJSONObject("error").getString("msg");

                        if (error_code == 0)

                        {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();

                    }


                }
            }
        }
                , new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        jsonObjectRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        requestQueue.add(jsonObjectRequest);

    }


    private void getProfile() {

        callProgressDialog();

        String url = getResources().getString(R.string.server_url_get_profile);

        String data = "{\"data\":[{\"user_id\":\"" + user_id + "\"}]}";
        JSONObject jsonObjectForrequest = null;

        try {

            jsonObjectForrequest = new JSONObject(data);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = null;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObjectForrequest, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                dialog.dismiss();

                if (response.has("success")) {

                    try {

                        String user = response.getJSONObject("success").getString("username");

                        String city = response.getJSONObject("success").getString("city");

                        String img = response.getJSONObject("success").getString("profile");

                        if (!img.equals("")) {

                            Picasso.with(context)
                                    .load(img)
                                    .transform(new CircleTransformPicasso(context))
                                    .placeholder(R.drawable.prof_bg)
                                    .into(img_profile);
                        } else {

                            Picasso.with(context)
                                    .load(R.drawable.prof_bg)
                                    .transform(new CircleTransformPicasso(context))
                                    .into(img_profile);
                        }

                        user_name.setText(user);
                        txt_city.setText(city);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {

                    try {

                        int error_code = response.getJSONObject("error").getInt("code");

                        String msg = response.getJSONObject("error").getString("msg");

                        if (error_code == 0)

                        {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            }
        }
                , new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        jsonObjectRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        requestQueue.add(jsonObjectRequest);
    }


    public void callProgressDialog() {
        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Loading..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}


