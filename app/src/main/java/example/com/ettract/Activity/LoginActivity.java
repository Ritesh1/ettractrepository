package example.com.ettract.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import example.com.ettract.R;
import example.com.ettract.Utility.Utility;

/**
 * Created by incipientinfo_pc4 on 28/12/16.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


    Toolbar toolbar;

    TextView toolbar_txt;

    TextView btn_login, edit_phone, edit_password;

    Context context;

    ProgressDialog dialog;

    CoordinatorLayout coordinateLayout;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        context = this;

        findViewById();

        sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);

        setListners();

    }

    private void setListners() {

        btn_login.setOnClickListener(this);
    }

    private void findViewById() {

        coordinateLayout = (CoordinatorLayout) findViewById(R.id.coordinateLayout);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar_txt = (TextView) findViewById(R.id.toolbar_txt);

        edit_phone = (TextView) findViewById(R.id.edit_phone);

        edit_password = (TextView) findViewById(R.id.edit_password);

        btn_login = (TextView) findViewById(R.id.btn_login);

        toolbar.setTitle("");
        toolbar_txt.setText("Login");
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(i);
                finish();

            }
        });

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_login:

                if (edit_phone.getText().toString().equals("") && edit_phone.getText().toString().isEmpty()) {

                    Utility.showSnackBarForCoordinateLayout(coordinateLayout, "please enter phone number");

                } else if (edit_password.getText().toString().equals("") && edit_password.getText().toString().isEmpty()) {

                    Utility.showSnackBarForCoordinateLayout(coordinateLayout, "please enter password");

                } else {

                    if (Utility.isInternetConnectionAvailable(context)) {
                        login();

                    } else {

                        Utility.showSnackBarForCoordinateLayout(coordinateLayout, "No Internet Connection");
                    }
                }

                break;
        }
    }

    private void login() {

        callProgressDialog();

        String url = getResources().getString(R.string.server_url_login);

        String data = "{\"data\":[{\"phoneno\":\"" + edit_phone.getText().toString() + "\",\"password\":\"" + edit_password.getText().toString() + "\",\"token_no\":\"" + sharedPreferences.getString("token_no", "") + "\"}]}";
        JSONObject jsonObjectForrequest = null;

        try {

            jsonObjectForrequest = new JSONObject(data);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = null;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObjectForrequest, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                dialog.dismiss();


                if (response.has("success"))

                {
                    saveDataToSharedPreference(response);

                    Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();


                } else

                {

                    try {

                        int error_code = response.getJSONObject("errors").getInt("code");

                        String msg = response.getJSONObject("errors").getString("msg");

                        if (error_code == 0)

                        {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }

                        if (error_code == 1)

                        {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }

                        if (error_code == 2)

                        {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }

                        if (error_code == 3)

                        {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }


                }
            }
        }
                , new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        jsonObjectRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        requestQueue.add(jsonObjectRequest);

    }


    private void saveDataToSharedPreference(JSONObject response) {

        SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        try {

            editor.putString("login_", "logged_in");
            editor.putString("id", response.getJSONObject("success").getString("id"));
            editor.putString("phoneno", response.getJSONObject("success").getString("phoneno"));
            editor.putBoolean("login", true);
            editor.commit();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void callProgressDialog() {
        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Loading..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}
