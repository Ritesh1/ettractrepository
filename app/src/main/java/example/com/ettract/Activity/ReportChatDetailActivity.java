package example.com.ettract.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import example.com.ettract.R;

/**
 * Created by incipientinfo_pc4 on 28/12/16.
 */

public class ReportChatDetailActivity extends AppCompatActivity {

    Toolbar toolbar;

    TextView toolbar_txt;

    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_report);

        context = this;

        findViewById();
    }

    private void findViewById() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar_txt = (TextView) findViewById(R.id.toolbar_txt);

        toolbar.setTitle("");
        toolbar_txt.setText("Key");

        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }

}
