package example.com.ettract.Activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import example.com.ettract.Adapter.ChatListAdapter;
import example.com.ettract.Modal.ChatMessage;
import example.com.ettract.R;
import example.com.ettract.Utility.Constant;
import example.com.ettract.Utility.Sqlite;
import example.com.ettract.Utility.Utility;

public class ChatActivityNew extends AppCompatActivity {

    private ListView chatListView;

    private EditText chatEditText1;

    Toolbar toolbar;
    ProgressDialog dialog;
    CoordinatorLayout coordinatorLayout;

    public static ArrayList<ChatMessage> chatMessageArrayList;

    private ImageView enterChatView1;

    BroadcastReceiver msgReceiver;

    String msg;

    Context context;

    public static boolean chat_active = false;

    SharedPreferences sharedPreferences;

    public static String receiver_id;

    String datetime;

    public static ChatListAdapter chatListAdapter;

    TextView  txt_report_toolbar_lable;

    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_new);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        name = getIntent().getStringExtra("user_name");

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout_chat_user_profile);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        txt_report_toolbar_lable = (TextView) findViewById(R.id.toolbar_txt);

        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        txt_report_toolbar_lable.setText(name);

        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        receiver_id = getIntent().getStringExtra("user_id");

        sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);

        context = this;

        Sqlite sqlite = new Sqlite(context);

        String query = "SELECT " + Sqlite.MESSAGE + "," + Sqlite.DATETIME + "," + Sqlite.WHO + " FROM " + Sqlite.DATABASE_TABLE + " where receiver_id = " + receiver_id + " ORDER BY datetime ASC";

        chatMessageArrayList = sqlite.fetchPerticularReceiverChat(query);

        chatListView = (ListView) findViewById(R.id.chat_list_view);

        chatEditText1 = (EditText) findViewById(R.id.chat_edit_text1);

        enterChatView1 = (ImageView) findViewById(R.id.enter_chat1);

        chatListAdapter = new ChatListAdapter(chatMessageArrayList, context);

        chatListView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        chatListView.setAdapter(chatListAdapter);

        chatEditText1.setOnKeyListener(keyListener);

        enterChatView1.setOnClickListener(clickListener);

        chatEditText1.addTextChangedListener(watcher1);

        getMessage();
    }


    private EditText.OnKeyListener keyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {

            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {

                EditText editText = (EditText) v;

                if (v == chatEditText1) {

                    sendMessage(editText.getText().toString(), Who.OTHER);
                }

                chatEditText1.setText("");

                return true;
            }
            return false;
        }
    };

    @Override
    public void onStart() {
        super.onStart();
        chat_active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        chat_active = false;
    }

    private ImageView.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (v == enterChatView1) {
                sendMessage(chatEditText1.getText().toString(), Who.OTHER);
            }

            String url = "http://dev5.demo4app.com/ettract/jsonapi/send_notification";

            JSONObject js = new JSONObject();
            try {
                JSONObject jsonobject_one = new JSONObject();

                jsonobject_one.put("message", chatEditText1.getText().toString());
                jsonobject_one.put("sender_id", sharedPreferences.getString("id", ""));
                jsonobject_one.put("receiver_id", receiver_id);

                JSONArray jsonArray = new JSONArray();

                jsonArray.put(0, jsonobject_one);

                js.put("data", jsonArray);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            JsonObjectRequest jsonObjectRequest = null;
            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, js, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    Sqlite sqlite = new Sqlite(context);

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");
                    datetime = sdf.format(new Date());

                    sqlite.insertMessage(sharedPreferences.getString("id", null), receiver_id, datetime, chatEditText1.getText().toString(), name, Constant.sender);

                    chatEditText1.getText().clear();

                }
            }
                    , new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    chatEditText1.getText().clear();

                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }
            };


            jsonObjectRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(context);

            requestQueue.add(jsonObjectRequest);

        }
    };

    private final TextWatcher watcher1 = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (chatEditText1.getText().toString().equals("")) {

            } else {
                enterChatView1.setImageResource(R.drawable.ic_chat_send);

            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.length() == 0) {
                enterChatView1.setImageResource(R.drawable.ic_chat_send);
            } else {
                enterChatView1.setImageResource(R.drawable.ic_chat_send_active);
            }
        }
    };

    private void sendMessage(final String messageText, final Who who) {
        if (messageText.trim().length() == 0)
            return;

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        datetime = sdf.format(new Date());

        final ChatMessage message = new ChatMessage();
        message.setMessageText(messageText);
        message.setMessageTime(datetime);
        message.setWho(Constant.sender);
        chatMessageArrayList.add(message);

        if (chatListAdapter != null)
            chatListAdapter.notifyDataSetChanged();

    }


    private void getMessage() {

        final ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);

        exec.schedule(new Runnable() {
            @Override
            public void run() {

                msgReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        msg = intent.getStringExtra("msg");

                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                        datetime = sdf.format(new Date());

                        final ChatMessage message = new ChatMessage();
                        message.setMessageText(msg);
                        message.setMessageTime(datetime);
                        message.setWho(Constant.receiver);
                        chatMessageArrayList.add(message);

                        ChatActivityNew.this.runOnUiThread(new Runnable() {
                            public void run() {
                                chatListAdapter.notifyDataSetChanged();
                            }
                        });

                    }
                };
                LocalBroadcastManager.getInstance(ChatActivityNew.this).registerReceiver(msgReceiver,
                        new IntentFilter("tokenReceiver"));


            }
        }, 100, TimeUnit.MILLISECONDS);
    }

}
