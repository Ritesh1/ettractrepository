package example.com.ettract.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import example.com.ettract.R;

/**
 * Created by incipientinfo_pc4 on 24/12/16.
 */

public class SplashActivity extends AppCompatActivity {


    SharedPreferences sharedPreferences;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);

        Thread t = new Thread() {

            public void run() {
                try {
                    sleep(3000);

                    if (sharedPreferences.contains("login_")) {
                        if (sharedPreferences.getString("login_", "").equals("logged_in")){
                            Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    } else {

                        Intent intent = new Intent(SplashActivity.this, SignUpActivity.class);
                        startActivity(intent);
                        finish();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();
    }
}
