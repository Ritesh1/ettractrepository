package example.com.ettract.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import example.com.ettract.R;


/**
 * Created by incipientinfo_pc4 on 27/12/16.
 */

public class CreateNewActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, OnMapReadyCallback {


    SeekBar mySeekBar;

    GoogleMap googleMap;

    Bitmap bm;

    public final static int IMG1 = 1;
    public final static int IMG2 = 2;
    public final static int IMG3 = 3;
    public final static int IMG4 = 4;

    LinearLayout add_photo;

    ImageView selected_img;

    Context context;

    Toolbar toolbar;

    TextView toolbar_txt;

    EditText edit_activity_title, edit_event_desc;

    ScrollView scrollview;

    ImageView img_game, img_face, img_deal, img_unspecified, img_strong;

    RadioGroup rg1, rg2;

    RadioButton rb_free, rb_$1, rb_$2, rb_$5;

    RadioButton rb2_free, rb2_$1, rb2_$2, rb2_$5;

    ProgressDialog dialog;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    String user_id, address;

    TextView tv_start;

    ImageView img_selected;

    TextView tv_selected;

    String event_location;

    TextView tv_cat;

    LinearLayout linear_photo;

    LinearLayout linear_img1, linear_img2, linear_img3;

    ImageView selected_img2, selected_img3, selected_img4;

    String strDate;

    String str_image_path, str_image_path2, str_image_path3, str_image_path4;

    //Define a request code to send to Google Play services
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    private GoogleApiClient mGoogleApiClient;

    private LocationRequest mLocationRequest;

    private double currentLatitude;

    private double currentLongitude;

    Bitmap bitmap2, bitmap3, bitmap4;

    TextView tv_remainig_hour, tv_close_shop, tv_cuurent_position, tv_hide_rich, tv_hour1, tv_km1, tv_money1, tv_money2;

    LinearLayout linear_by;

    TextView txt_time_remaining;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_new_activity);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        context = this;

        mySeekBar = (SeekBar) findViewById(R.id.seekBar);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)
                .setFastestInterval(1 * 1000);

        initilizeMap();

        findViewById();

        setListners();
    }


    protected void onResume() {
        super.onResume();

        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {

        super.onPause();
        Log.v(this.getClass().getSimpleName(), "onPause()");
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

    }


    @Override
    public void onConnected(Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (com.google.android.gms.location.LocationListener) this);

        } else {

            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
        }

        Geocoder geocoder;

        List<Address> addresses;

        geocoder = new Geocoder(this, Locale.getDefault());

        try {

            addresses = geocoder.getFromLocation(currentLatitude, currentLongitude, 1);

            event_location = addresses.get(0).getSubLocality();

            tv_cuurent_position.setText("in your current position " + event_location);

            address = addresses.get(0).getLocality();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        if (connectionResult.hasResolution()) {
            try {

                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

            } catch (IntentSender.SendIntentException e) {

                e.printStackTrace();
            }

        }
    }


    @Override
    public void onLocationChanged(Location location) {

        currentLatitude = location.getLatitude();

        currentLongitude = location.getLongitude();

        Geocoder geocoder;

        List<Address> addresses;

        geocoder = new Geocoder(this, Locale.getDefault());

        try {

            addresses = geocoder.getFromLocation(currentLatitude, currentLongitude, 1);

            event_location = addresses.get(0).getSubLocality();

            tv_cuurent_position.setText("in your current position " + event_location);

            address = addresses.get(0).getLocality();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


    private void findViewById() {

        sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        editor = sharedPreferences.edit();

        txt_time_remaining = (TextView) findViewById(R.id.txt_time_remaining);

        user_id = sharedPreferences.getString("id", "");

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        add_photo = (LinearLayout) findViewById(R.id.add_photo);

        selected_img = (ImageView) findViewById(R.id.selected_img);

        toolbar_txt = (TextView) findViewById(R.id.toolbar_txt);

        img_deal = (ImageView) findViewById(R.id.img_deal);

        img_face = (ImageView) findViewById(R.id.img_face);

        img_game = (ImageView) findViewById(R.id.img_game);

        img_strong = (ImageView) findViewById(R.id.img_strong);

        img_unspecified = (ImageView) findViewById(R.id.img_unspecified);

        edit_activity_title = (EditText) findViewById(R.id.edit_activity_title);

        edit_event_desc = (EditText) findViewById(R.id.edit_event_desc);

        tv_start = (TextView) findViewById(R.id.tv_start);

        toolbar.setTitle("");
        toolbar_txt.setText("Create New Activity");
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(CreateNewActivity.this, HomeActivity.class);
                startActivity(i);
                finish();

            }
        });

        mySeekBar = (SeekBar) findViewById(R.id.seekBar);

        mySeekBar.getProgressDrawable().setColorFilter(Color.parseColor("#2EA5DE"), PorterDuff.Mode.SRC_IN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mySeekBar.getThumb().setColorFilter(Color.parseColor("#2EA5DE"), PorterDuff.Mode.SRC_IN);
        }

        mySeekBar.setMax(4 * 4);
        mySeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

                int hours = progress / 4;
                int minutes = (progress % 4) * 15;

                txt_time_remaining.setText(String.valueOf(hours) + "hr " + String.valueOf(minutes) + "min");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        scrollview = (ScrollView) findViewById(R.id.scrollview);

        ImageView transparentImageView = (ImageView) findViewById(R.id.transparent_image);

        transparentImageView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int action = event.getAction();

                switch (action) {

                    case MotionEvent.ACTION_DOWN:

                        scrollview.requestDisallowInterceptTouchEvent(true);
                        return false;

                    case MotionEvent.ACTION_UP:

                        scrollview.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:

                        scrollview.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });


        rg1 = (RadioGroup) findViewById(R.id.rg1);

        rg2 = (RadioGroup) findViewById(R.id.rg2);

        rb_free = (RadioButton) findViewById(R.id.rb_free);

        rb_$1 = (RadioButton) findViewById(R.id.rb_$1);

        rb_$2 = (RadioButton) findViewById(R.id.rb_$2);

        rb_$5 = (RadioButton) findViewById(R.id.rb_$5);

        rb2_$1 = (RadioButton) findViewById(R.id.rb2_$1);

        rb2_$2 = (RadioButton) findViewById(R.id.rb2_$2);

        rb2_$5 = (RadioButton) findViewById(R.id.rb2_$5);

        rb2_free = (RadioButton) findViewById(R.id.rb2_free);

        img_selected = (ImageView) findViewById(R.id.img_selected);

        tv_selected = (TextView) findViewById(R.id.tv_selected);

        tv_cat = (TextView) findViewById(R.id.tv_cat);

        linear_photo = (LinearLayout) findViewById(R.id.liner_photo);

        selected_img3 = (ImageView) findViewById(R.id.selected_img3);

        selected_img2 = (ImageView) findViewById(R.id.selected_img2);

        selected_img4 = (ImageView) findViewById(R.id.selected_img4);

        linear_img1 = (LinearLayout) findViewById(R.id.linear_img1);

        linear_img2 = (LinearLayout) findViewById(R.id.linear_img2);

        linear_img3 = (LinearLayout) findViewById(R.id.linear_img3);

        tv_remainig_hour = (TextView) findViewById(R.id.tv_remainig_hour);

        tv_close_shop = (TextView) findViewById(R.id.tv_close_shop);

        tv_cuurent_position = (TextView) findViewById(R.id.tv_cuurent_position);

        tv_hide_rich = (TextView) findViewById(R.id.tv_hide_rich);

        tv_hour1 = (TextView) findViewById(R.id.tv_hour1);

        tv_km1 = (TextView) findViewById(R.id.tv_km1);

        tv_money1 = (TextView) findViewById(R.id.tv_money1);

        tv_money2 = (TextView) findViewById(R.id.tv_money2);

        linear_by = (LinearLayout) findViewById(R.id.linear_by);

        rg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                rb_free = (RadioButton) findViewById(checkedId);

                rb_$1 = (RadioButton) findViewById(checkedId);

                rb_$2 = (RadioButton) findViewById(checkedId);

                rb_$5 = (RadioButton) findViewById(checkedId);

                if (rb_free.isChecked()) {

                    tv_remainig_hour.setText("you have " + rb_free.getText().toString() + " left today");
                    tv_hour1.setText("");
                }

                if (rb_$1.isChecked()) {

                    tv_remainig_hour.setText("you have " + rb_$1.getText().toString() + " left today");
                    tv_hour1.setText("+" + rb_$1.getText().toString());
                }

                if (rb_$2.isChecked()) {

                    tv_remainig_hour.setText("you have " + rb_$2.getText().toString() + " left today");
                    tv_hour1.setText("+ " + rb_$2.getText().toString());
                }

                if (rb_$5.isChecked()) {

                    tv_remainig_hour.setText("you have " + rb_$5.getText().toString() + " left today");
                    tv_hour1.setText("+ " + rb_$5.getText().toString());
                }


            }
        });


        rg2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {

                rb2_free = (RadioButton) findViewById(checkedId);

                rb2_$1 = (RadioButton) findViewById(checkedId);

                rb2_$2 = (RadioButton) findViewById(checkedId);

                rb2_$5 = (RadioButton) findViewById(checkedId);


                if (rb2_free.isChecked()) {

                    tv_km1.setText("");
                }

                if (rb2_$1.isChecked()) {

                    tv_km1.setText(rb2_$1.getText().toString());

                }

                if (rb2_$2.isChecked()) {

                    tv_km1.setText(rb2_$2.getText().toString());

                }

                if (rb2_$5.isChecked()) {

                    tv_km1.setText(rb2_$5.getText().toString());

                }


            }
        });

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        strDate = sdfDate.format(now);
    }

    private void setListners() {

        add_photo.setOnClickListener(this);

        img_deal.setOnClickListener(this);

        img_strong.setOnClickListener(this);

        img_game.setOnClickListener(this);

        img_unspecified.setOnClickListener(this);

        img_face.setOnClickListener(this);

        tv_start.setOnClickListener(this);

        linear_img1.setOnClickListener(this);

        linear_img2.setOnClickListener(this);

    }

    private void initilizeMap() {

        if (googleMap == null) {

            MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);

            if (googleMap == null) {
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.add_photo:

                linear_photo.setVisibility(View.VISIBLE);

                gallaryIntentCall(IMG1);

                break;

            case R.id.linear_img1:

                selected_img2.setVisibility(View.VISIBLE);

                gallaryIntentCall(IMG2);
                break;

            case R.id.linear_img2:
                selected_img3.setVisibility(View.VISIBLE);

                gallaryIntentCall(IMG3);

                break;

            case R.id.linear_img3:
                selected_img4.setVisibility(View.VISIBLE);

                gallaryIntentCall(IMG4);
                break;

            case R.id.tv_start:

                addNewActivity();

                break;


            case R.id.img_game:

                img_selected.setVisibility(View.VISIBLE);
                img_selected.setImageResource(R.drawable.game);
                tv_selected.setText("game");
                tv_cat.setText("2");

                break;

            case R.id.img_deal:

                img_selected.setVisibility(View.VISIBLE);
                img_selected.setImageResource(R.drawable.deal);
                tv_selected.setText("deal");
                tv_cat.setText("4");

                break;

            case R.id.img_unspecified:

                img_selected.setVisibility(View.VISIBLE);
                img_selected.setImageResource(R.drawable.unspecified);
                tv_selected.setText("unspecified");
                tv_cat.setText("5");

                break;

            case R.id.img_face:

                img_selected.setVisibility(View.VISIBLE);
                img_selected.setImageResource(R.drawable.face);
                tv_selected.setText("face");
                tv_cat.setText("3");

                break;

            case R.id.img_strong:

                img_selected.setVisibility(View.VISIBLE);
                img_selected.setImageResource(R.drawable.strong);
                tv_selected.setText("strong");
                tv_cat.setText("1");

                break;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case IMG1:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    galleryIntent(IMG1);
                }

                break;

            case IMG2:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    galleryIntent(IMG2);
                }

                break;

            case IMG3:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    galleryIntent(IMG3);
                }

                break;

            case IMG4:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    galleryIntent(IMG4);
                }

                break;
        }
    }


    private void galleryIntent(int requestCodeForImg1) {

        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.setType("image/*");

        pickPhoto.putExtra("crop", "true");
        pickPhoto.putExtra("aspectX", 0);
        pickPhoto.putExtra("aspectY", 0);
        pickPhoto.putExtra("outputX", 200);
        pickPhoto.putExtra("outputY", 150);

        pickPhoto.putExtra("return-data", true);

        startActivityForResult(pickPhoto, requestCodeForImg1);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == IMG1)

            {
                Bundle extras = data.getExtras();
                if (extras != null) {
                    bm = extras.getParcelable("data");
                    selected_img.setImageBitmap(bm);
                }

            } else if (requestCode == IMG2) {

                Bundle extras = data.getExtras();
                if (extras != null) {
                    bitmap2 = extras.getParcelable("data");
                    selected_img2.setImageBitmap(bitmap2);
                }

            } else if (requestCode == IMG3) {

                Bundle extras = data.getExtras();
                if (extras != null) {
                    bitmap3 = extras.getParcelable("data");
                    selected_img3.setImageBitmap(bitmap3);
                }


            } else if (requestCode == IMG4) {

                Bundle extras = data.getExtras();
                if (extras != null) {
                    bitmap4 = extras.getParcelable("data");
                    selected_img4.setImageBitmap(bitmap4);
                }

            }
        }

    }

    private void gallaryIntentCall(int requestCodeForImg1) {

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            requestPermission(requestCodeForImg1);

        } else {

            galleryIntent(requestCodeForImg1);
        }
    }

    private void requestPermission(int requestCodeForImg1) {

        String[] PERMISSIONS_CALL = {Manifest.permission.READ_EXTERNAL_STORAGE};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            requestPermissions(PERMISSIONS_CALL, requestCodeForImg1);
        }
    }


    private void addNewActivity() {

        callProgressDialog();

        if (bm != null)

        {
            str_image_path = convertImageToString(bm);
        }


        if (bitmap2 != null)

        {
            str_image_path2 = convertImageToString(bitmap2);
        }


        if (bitmap3 != null)

        {
            str_image_path3 = convertImageToString(bitmap3);
        }


        if (bitmap4 != null)

        {
            str_image_path4 = convertImageToString(bitmap4);
        }

        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        String time;
        if (txt_time_remaining.getText().toString().equals("0hr 0min")) {

            time = "4";
        } else {

            time = txt_time_remaining.getText().toString();
        }


        String url = getResources().getString(R.string.server_url_add_new_activity);

        String data = "{\"data\":[{\"user_id\":\"" + user_id + "\",\"title\":\"" + edit_activity_title.getText().toString() + "\",\"event_description\":\"" + edit_event_desc.getText().toString() + "\",\"category_id\":\"" + tv_cat.getText().toString() + "\",\"event_location\":\"" + event_location + "\",\"address\":\"" + address + "\",\"lat\":\"" + currentLatitude + "\",\"long\":\"" + currentLongitude + "\",\"no_hours\":\"" + time + "\",\"start_time\":\"" + strDate + "\",\"activity_range\":\"5\",\"images\": [{\"link\":\"" + str_image_path + "\"}, {\"link\":\"" + str_image_path2 + "\"},{\"link\":\"" + str_image_path3 + "\"}]}]}";

        JSONObject jsonObjectForrequest = null;

        try {

            jsonObjectForrequest = new JSONObject(data);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = null;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObjectForrequest, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                dialog.dismiss();

                if (response.has("success"))

                {

                    Toast.makeText(context, "Activity created", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(CreateNewActivity.this, HomeActivity.class);

                    startActivity(i);
                    finish();


                } else

                {

                    try {

                        int error_code = response.getJSONObject("error").getInt("code");

                        String msg = response.getJSONObject("error").getString("msg");


                        if (error_code == 1)

                        {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        } else if (error_code == 2)

                        {
                            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();

                    }


                }
            }
        }
                , new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        jsonObjectRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(context);

        requestQueue.add(jsonObjectRequest);

    }


    private String convertImageToString(Bitmap bitmap) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public void callProgressDialog() {

        dialog = new ProgressDialog(context);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("Loading..");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.googleMap = googleMap;

        if (googleMap == null) {

            Toast.makeText(getApplicationContext(), "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap.setMyLocationEnabled(true);
    }

}
