package example.com.ettract.Modal;

/**
 * Created by madhur on 17/01/15.
 */
public class ChatMessage {

    private String messageText;

    private String who;

    private String messageTime;

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public String getMessageText() {

        return messageText;
    }

    public String getWho() {
        return who;
    }
}
