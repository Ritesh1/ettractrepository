package example.com.ettract.Utility;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import example.com.ettract.R;

/**
 * Created by incipientinfo_pc4 on 28/12/16.
 */

public class Utility {


    public static boolean isInternetConnectionAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo network = connectivityManager.getActiveNetworkInfo();
        if (network == null) {
            return false;
        } else
            return true;
    }


    public static void showSnackBarForCoordinateLayout(CoordinatorLayout linearLayout, String msg) {

        Snackbar snackbar = null;
        snackbar = Snackbar.make(linearLayout, msg, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }

        textView.setTextColor(Color.WHITE);

        snackbar.show();

    }

    public static int getPxFromDp(Context context, float dpUnits) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpUnits, context.getResources().getDisplayMetrics());
    }

    public static void checkDeviceAndSetProfileImg(Context context, String user_profile, ImageView iv_user_profile_img) {

        int density = context.getResources().getDisplayMetrics().densityDpi;

        switch (density) {

            case DisplayMetrics.DENSITY_MEDIUM:

                Picasso.with(context).load(user_profile).placeholder(R.drawable.prof_bg).transform(new CircleTransformPicasso(context, 1)).resize(150, 200).into(iv_user_profile_img);

                break;
            case DisplayMetrics.DENSITY_HIGH:

                Picasso.with(context).load(user_profile).placeholder(R.drawable.prof_bg).transform(new CircleTransformPicasso(context, 1)).resize(200, 250).into(iv_user_profile_img);

                break;
            case DisplayMetrics.DENSITY_XHIGH:

                Picasso.with(context).load(user_profile).placeholder(R.drawable.prof_bg).transform(new CircleTransformPicasso(context, 1)).resize(250, 300).into(iv_user_profile_img);

                break;
            case DisplayMetrics.DENSITY_XXHIGH:

                Picasso.with(context).load(user_profile).placeholder(R.drawable.prof_bg).transform(new CircleTransformPicasso(context, 1)).resize(300, 350).into(iv_user_profile_img);

                break;
            case DisplayMetrics.DENSITY_XXXHIGH:

                Picasso.with(context).load(user_profile).placeholder(R.drawable.prof_bg).transform(new CircleTransformPicasso(context, 1)).resize(350, 400).into(iv_user_profile_img);

                break;
        }
    }

    public static void checkDeviceAndSetSmallProfileImg(Context context, String user_profile, ImageView iv_user_profile_img) {

        int density = context.getResources().getDisplayMetrics().densityDpi;

        switch (density) {

            case DisplayMetrics.DENSITY_MEDIUM:

                Picasso.with(context).load(user_profile).placeholder(R.drawable.small_prof_bg).transform(new CircleTransformPicasso(context, 1)).resize(50, 0).into(iv_user_profile_img);

                break;
            case DisplayMetrics.DENSITY_HIGH:

                Picasso.with(context).load(user_profile).placeholder(R.drawable.small_prof_bg).transform(new CircleTransformPicasso(context, 1)).resize(80, 0).into(iv_user_profile_img);

                break;
            case DisplayMetrics.DENSITY_XHIGH:

                Picasso.with(context).load(user_profile).placeholder(R.drawable.small_prof_bg).transform(new CircleTransformPicasso(context, 1)).resize(120, 0).into(iv_user_profile_img);

                break;
            case DisplayMetrics.DENSITY_XXHIGH:

                Picasso.with(context).load(user_profile).placeholder(R.drawable.small_prof_bg).transform(new CircleTransformPicasso(context, 1)).resize(170, 0).into(iv_user_profile_img);

                break;
            case DisplayMetrics.DENSITY_XXXHIGH:

                Picasso.with(context).load(user_profile).placeholder(R.drawable.small_prof_bg).transform(new CircleTransformPicasso(context, 1)).resize(230, 0).into(iv_user_profile_img);

                break;
        }
    }


}
