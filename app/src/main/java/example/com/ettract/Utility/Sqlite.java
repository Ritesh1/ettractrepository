package example.com.ettract.Utility;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import example.com.ettract.Modal.ChatHistoryModel;
import example.com.ettract.Modal.ChatMessage;

/**
 * Created by IncipientinfoPC 2 on 18-Mar-2016.
 */
public class Sqlite extends SQLiteOpenHelper {

    public static final String DATABASE_PATH = "/data/data/com.humanity/databases/";
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "db_chat";
    public static final String DATABASE_TABLE = "tbl_chat";
    public static final String ID = "dest_id";
    public static final String SENDER_ID = "sender_id";
    public static final String RECEIVER_ID = "receiver_id";
    public static final String MESSAGE = "msg";
    public static final String DATETIME = "datetime";
    public static final String WHO = "who";
    public static final String NAME = "name";

    Context context;

    public Sqlite(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE IF NOT EXISTS " + DATABASE_TABLE + "(" +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL " + "," +
                SENDER_ID + " TEXT " + "," +
                RECEIVER_ID + " TEXT " + "," +
                MESSAGE + " TEXT " + "," +
                DATETIME + " TEXT " + "," +
                NAME + " TEXT " + "," +
                WHO + " INTEGER " + ")");
    }

    public void insertMessage(String sender_id, String receiver_id, String datetime, String message, String user_name, String who)

    {
        SQLiteOpenHelper sqLiteOpenHelper = new Sqlite(context);

        SQLiteDatabase sqLiteDatabase = sqLiteOpenHelper.getWritableDatabase();

        String insertQuery = "INSERT INTO " + DATABASE_TABLE + "(" + SENDER_ID + "," + RECEIVER_ID + "," + MESSAGE + "," + DATETIME + "," + NAME + "," + WHO + ") VALUES ('" + sender_id + "','" + receiver_id + "','" + message + "','" + datetime + "','" + user_name + "','" + who + "')";
        sqLiteDatabase.execSQL(insertQuery);
    }

    public ArrayList<ChatHistoryModel> fetchChat(String query) {

        SQLiteOpenHelper sqLiteOpenHelper = new Sqlite(context);

        SQLiteDatabase sqLiteDatabase = sqLiteOpenHelper.getWritableDatabase();

        ArrayList<ChatHistoryModel> chatHistoryModelArrayList = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        if (cursor.moveToFirst()) {

            do {

                ChatHistoryModel chatHistoryModel = new ChatHistoryModel();

                String receiver_id = cursor.getString(cursor.getColumnIndex(RECEIVER_ID));

                String name = cursor.getString(cursor.getColumnIndex(NAME));

                String msg = cursor.getString(cursor.getColumnIndex(MESSAGE));

                chatHistoryModel.setReceiver_id(receiver_id);

                chatHistoryModel.setMsg(msg);

                chatHistoryModel.setName(name);

                chatHistoryModelArrayList.add(chatHistoryModel);

            } while (cursor.moveToNext());
        }

        return chatHistoryModelArrayList;
    }

    public ArrayList<ChatMessage> fetchPerticularReceiverChat(String query) {

        SQLiteOpenHelper sqLiteOpenHelper = new Sqlite(context);

        SQLiteDatabase sqLiteDatabase = sqLiteOpenHelper.getWritableDatabase();

        ArrayList<ChatMessage> chatMessageArrayList = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        if (cursor.moveToFirst()) {

            do {

                ChatMessage chatMessage = new ChatMessage();

                String who = cursor.getString(cursor.getColumnIndex(WHO));

                String msg = cursor.getString(cursor.getColumnIndex(MESSAGE));

                String datetime = cursor.getString(cursor.getColumnIndex(DATETIME));

                chatMessage.setMessageText(msg);

                chatMessage.setWho(who);

                chatMessage.setMessageTime(datetime);

                chatMessageArrayList.add(chatMessage);

            } while (cursor.moveToNext());
        }

        return chatMessageArrayList;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (newVersion > oldVersion)

        {
        }

    }

    public void deleteDestData(Integer dest_id) {

        SQLiteOpenHelper sqLiteOpenHelper = new Sqlite(context);

        SQLiteDatabase sqLiteDatabase = sqLiteOpenHelper.getWritableDatabase();

        String query = "DELETE FROM " + DATABASE_TABLE + " WHERE dest_id='" + dest_id + "'";
        sqLiteDatabase.execSQL(query);
    }
}
