package example.com.ettract.Utility;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import example.com.ettract.Activity.ChatActivity;
import example.com.ettract.Activity.ChatActivityNew;
import example.com.ettract.R;

/**
 * Created by Belal on 5/27/2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    SharedPreferences sharedPreferences;

    String datetime;

    public static boolean check_msg = false;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Map<String, String> test5 = remoteMessage.getData();

        int size = test5.size();


        if (size != 0)

        {
            Collection<String> ts = test5.values();

            Iterator<String> stst = ts.iterator();

            String id = stst.next();

            String name = stst.next();

            sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");
            datetime = sdf.format(new Date());

            Sqlite sqlite = new Sqlite(getApplicationContext());
            sqlite.insertMessage(sharedPreferences.getString("id", null), id, datetime, remoteMessage.getNotification().getBody().toString(), name, Constant.receiver);

            if (ChatActivityNew.chat_active)

            {
                if (id.equals(ChatActivityNew.receiver_id))

                {
                    String msg = remoteMessage.getNotification().getBody();

                    final Intent intent = new Intent("tokenReceiver");
                    final LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
                    intent.putExtra("msg", msg);
                    broadcastManager.sendBroadcast(intent);

                } else

                {
                    sendNotification(remoteMessage.getNotification().getBody());
                }

            } else {

                if (ChatActivity.main_active)

                {

                    final Intent intent = new Intent("tokenReceiver");
                    final LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
                    intent.putExtra("msg", remoteMessage.getNotification().getBody());
                    broadcastManager.sendBroadcast(intent);
                }
                sendNotification(remoteMessage.getNotification().getBody());
            }
        } else

        {
            sendNotification(remoteMessage.getNotification().getBody());
        }
    }


    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(largeIcon)
                .setContentTitle("Humanity")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());

    }
}