package example.com.ettract.Utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.squareup.picasso.Transformation;

import example.com.ettract.R;

/**
 * Created by IncipientinfoPC 2 on 07-May-2016.
 */
public class CircleTransformPicasso implements Transformation {

    int i;

    Context context;

    public CircleTransformPicasso(Context context, int i) {

        this.i = i;

        this.context = context;
    }

    public CircleTransformPicasso(Context context) {
        this.context = context;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        int size = Math.min(source.getWidth(), source.getHeight());

        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;

        Bitmap bitmap = null;

        Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
        if (squaredBitmap != source) {
            source.recycle();
        }

        try {
            bitmap = Bitmap.createBitmap(size, size, source.getConfig());
        } catch (Exception e) {

        } finally {

            if (bitmap == null)

            {
                bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.camera);

                bitmap = Bitmap.createBitmap(size, size, bitmap.getConfig());
            }
        }
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(squaredBitmap,
                BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);

        float r = size / 2f;
        canvas.drawCircle(r, r, r, paint);

        squaredBitmap.recycle();

        if (i == 1)

        {
            Paint paint1 = new Paint();
            paint1.setColor(Color.parseColor("#000000"));
            paint1.setStyle(Paint.Style.STROKE);
            paint1.setAntiAlias(true);
            paint1.setStrokeWidth(3);
            canvas.drawCircle(r, r, r - 1, paint1);
        }
        return bitmap;
    }

    @Override
    public String key() {
        return "circle";
    }
}