package example.com.ettract.Utility;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by incipientinfo_pc4 on 23/8/16.
 */
public class ClusterMarkerLocation implements ClusterItem {

    private LatLng latLng;

    double lat;

    double lng;

    String leaf_id;

    String user_id;

    String user_name;

    String user_profile;

    String leaf_title;

    String leaf_category;

    int marker_position;

    public int getMarker_position() {
        return marker_position;
    }

    public void setMarker_position(int marker_position) {
        this.marker_position = marker_position;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getLeaf_id() {
        return leaf_id;
    }

    public void setLeaf_id(String leaf_id) {
        this.leaf_id = leaf_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_profile() {
        return user_profile;
    }

    public void setUser_profile(String user_profile) {
        this.user_profile = user_profile;
    }

    public String getLeaf_title() {
        return leaf_title;
    }

    public void setLeaf_title(String leaf_title) {
        this.leaf_title = leaf_title;
    }

    public String getLeaf_category() {
        return leaf_category;
    }

    public void setLeaf_category(String leaf_category) {
        this.leaf_category = leaf_category;
    }

    public ClusterMarkerLocation(LatLng latLng1, int marker_position1) {

        marker_position = marker_position1;
        latLng = latLng1;
    }

    @Override
    public LatLng getPosition() {
        return latLng;
    }

    public void setPosition(LatLng position) {
        this.latLng = position;
    }
}