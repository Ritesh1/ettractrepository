package example.com.ettract.Utility;


import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by incipientinfo_pc5 on 3/10/16.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    @Override
    public void onTokenRefresh() {

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        editor = sharedPreferences.edit();

        editor.putString("token_no", refreshedToken);
        editor.commit();

        sendRegistrationToServer(refreshedToken);

        Log.i("Refreshed token: ", refreshedToken);

        final Intent intent = new Intent("tokenReceiver");
        final LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        intent.putExtra("token", refreshedToken);
        broadcastManager.sendBroadcast(intent);

    }

    private void sendRegistrationToServer(final String token) {

    }
}
