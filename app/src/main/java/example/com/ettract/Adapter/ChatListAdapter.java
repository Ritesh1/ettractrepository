package example.com.ettract.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import example.com.ettract.Modal.ChatMessage;
import example.com.ettract.R;
import example.com.ettract.Utility.Constant;

/**
 * Created by madhur on 17/01/15.
 */
public class ChatListAdapter extends BaseAdapter {

    private ArrayList<ChatMessage> chatMessageArrayList;

    private Context context;

    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("HH:mm");

    public ChatListAdapter(ArrayList<ChatMessage> chatMessageArrayList, Context context) {

        this.chatMessageArrayList = chatMessageArrayList;
        this.context = context;

    }

    @Override
    public int getCount() {
        return chatMessageArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = null;

        TextView messageTextView = null;
        TextView timeTextView = null;

        if (chatMessageArrayList.get(position).getWho().equals(Constant.sender)) {

            v = LayoutInflater.from(context).inflate(R.layout.adapter_sender, null, false);

            messageTextView = (TextView) v.findViewById(R.id.message_text);
            timeTextView = (TextView) v.findViewById(R.id.time_text);

            messageTextView.setText(chatMessageArrayList.get(position).getMessageText().toString());
            timeTextView.setText(chatMessageArrayList.get(position).getMessageTime().toString());

        } else if (chatMessageArrayList.get(position).getWho().equals(Constant.receiver)) {

            v = LayoutInflater.from(context).inflate(R.layout.adapter_receiver, null, false);

            messageTextView = (TextView) v.findViewById(R.id.message_text);
            timeTextView = (TextView) v.findViewById(R.id.time_text);

            messageTextView.setText(chatMessageArrayList.get(position).getMessageText().toString());
            timeTextView.setText(chatMessageArrayList.get(position).getMessageTime().toString());

           /* if (message.getMessageStatus() == Status.DELIVERED) {
                holder2.messageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_double_tick));
            } else if (message.getMessageStatus() == Status.SENT) {
                holder2.messageStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_single_tick));

            }*/

        }

        return v;
    }
}
