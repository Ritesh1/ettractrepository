package example.com.ettract.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import example.com.ettract.Modal.ChatHistoryModel;
import example.com.ettract.R;


/**
 * Created by incipientinfo_pc4 on 20/10/16.
 */
public class ChatHistoryAdapter extends BaseAdapter {

    ArrayList<ChatHistoryModel> chatHistoryModelArrayList;

    Context context;

    LayoutInflater layoutInflator;


    public ChatHistoryAdapter(Context context, ArrayList<ChatHistoryModel> chatHistoryModelArrayList) {

        this.context = context;

        this.chatHistoryModelArrayList = chatHistoryModelArrayList;

        layoutInflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return chatHistoryModelArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View convertView;

        if (view == null)

        {
            convertView = layoutInflator.inflate(R.layout.adapter_chat, null);

        } else

        {
            convertView = view;
        }

        TextView txt_name = (TextView) convertView.findViewById(R.id.txt_name);

        TextView txt_msg = (TextView) convertView.findViewById(R.id.txt_msg);

        txt_name.setText(chatHistoryModelArrayList.get(i).getName());

        txt_msg.setText(chatHistoryModelArrayList.get(i).getMsg());

        return convertView;
    }
}
