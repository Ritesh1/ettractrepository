package example.com.ettract.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import example.com.ettract.R;

/**
 * Created by incipientinfo_pc4 on 29/12/16.
 */

public class MapListDataAdapter extends BaseAdapter {

    Context context;

    JSONArray maplist_data_array;

    JSONArray img_list_array;

    String user_id, title, time, location, profile, user_name, desc, tv_category;


    public MapListDataAdapter(Context context, JSONArray maplist_data_array, JSONArray img_list_array) {

        this.context = context;

        this.maplist_data_array = maplist_data_array;

        this.img_list_array = img_list_array;

    }

    @Override
    public int getCount() {

        return maplist_data_array.length();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        TextView tv_title;

        TextView tv_desc;

        ImageView img_list;

        TextView tv_time;

        LinearLayout list_detail;

        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView = inflater.inflate(R.layout.row_list, viewGroup, false);

        tv_title = (TextView) convertView.findViewById(R.id.tv_title);

        tv_desc = (TextView) convertView.findViewById(R.id.tv_desc);

        tv_time = (TextView) convertView.findViewById(R.id.tv_time);

        img_list = (ImageView) convertView.findViewById(R.id.img_list);

        list_detail = (LinearLayout) convertView.findViewById(R.id.list_detail);

        try {

            user_id = maplist_data_array.getJSONObject(i).getString("user_id");

            title = maplist_data_array.getJSONObject(i).getString("title");

            desc = maplist_data_array.getJSONObject(i).getString("event_description");

            time = maplist_data_array.getJSONObject(i).getString("remaining_hours");

            profile = maplist_data_array.getJSONObject(i).getString("user_profile");

            user_name = maplist_data_array.getJSONObject(i).getString("user_name");

            location = maplist_data_array.getJSONObject(i).getString("address");

            tv_category = maplist_data_array.getJSONObject(i).getString("category_id");

            tv_title.setText(title);

            tv_desc.setText(desc);

            String[] parts = time.split(":");
            String part1 = parts[0];
            String part2 = parts[1];

            tv_time.setText(part1 + "h " + part2 + "min" + " left");

            JSONObject result = maplist_data_array.getJSONObject(i);
            img_list_array = result.getJSONArray("images");

            String url = img_list_array.getJSONObject(0).getString("link");

            Picasso.with(context)
                    .load(url)
                    .into(img_list);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        if (tv_category.equals("1")) {
            tv_time.setCompoundDrawablesWithIntrinsicBounds(R.drawable.strong, 0, 0, 0);
        }

        if (tv_category.equals("2"))

        {
            tv_time.setCompoundDrawablesWithIntrinsicBounds(R.drawable.game, 0, 0, 0);
        }

        if (tv_category.equals("3"))

        {
            tv_time.setCompoundDrawablesWithIntrinsicBounds(R.drawable.face, 0, 0, 0);
        }
        if (tv_category.equals("4"))

        {
            tv_time.setCompoundDrawablesWithIntrinsicBounds(R.drawable.deal, 0, 0, 0);
        }
        if (tv_category.equals("5"))

        {
            tv_time.setCompoundDrawablesWithIntrinsicBounds(R.drawable.unspecified, 0, 0, 0);
        }

        list_detail.setTag(i);

        return convertView;
    }
}

