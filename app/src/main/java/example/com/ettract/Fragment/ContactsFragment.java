package example.com.ettract.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import example.com.ettract.Activity.ChatActivity;
import example.com.ettract.Adapter.ContactsListAdapter;
import example.com.ettract.Modal.ContactModel;
import example.com.ettract.R;

/**
 * Created by incipientinfo_pc5 on 12/10/16.
 */
public class ContactsFragment extends Fragment implements AdapterView.OnItemClickListener {

    Context context;

    ListView listView_contacts;

    ContactsListAdapter listAdapter;

    JSONArray jsonArray;

    SharedPreferences sharedPreferences;

    public ArrayList<ContactModel> array_contactModel = new ArrayList<ContactModel>();

    ContactModel contactModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contacts, container, false);

        context = container.getContext();

        sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);

        listView_contacts = (ListView) view.findViewById(R.id.listView_contacts);


        String url = "http://dev5.demo4app.com/humanity/jsonapi/contact_list";

        JSONObject js = new JSONObject();
        try {
            JSONObject jsonobject_one = new JSONObject();

            jsonobject_one.put("user_id", sharedPreferences.getString("id", null));

            JSONArray jsonArray = new JSONArray();

            jsonArray.put(0, jsonobject_one);

            js.put("data", jsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = null;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, js, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                try {
                    jsonArray = response.getJSONArray("success");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        contactModel = new ContactModel();

                        contactModel.setId(jsonArray.getJSONObject(i).getString("id"));
                        contactModel.setName(jsonArray.getJSONObject(i).getString("name"));
                        contactModel.setProfile_pic(jsonArray.getJSONObject(i).getString("profile"));
                        array_contactModel.add(contactModel);
                    }
                    listAdapter = new ContactsListAdapter(context, array_contactModel);
                    listView_contacts.setAdapter(listAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
                , new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };


        jsonObjectRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        requestQueue.add(jsonObjectRequest);

        listView_contacts.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        contactModel = (ContactModel) ContactsListAdapter.jsonArray.get(i);

        String name = contactModel.getName();
        String id = contactModel.getId();

        Intent intent = new Intent(this.getActivity(), ChatActivity.class);
        intent.putExtra("user_name", name);
        intent.putExtra("id", id);
        startActivity(intent);
        getActivity().finish();

    }
}