package example.com.ettract.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

import example.com.ettract.Activity.ChatActivity;
import example.com.ettract.Adapter.ChatHistoryAdapter;
import example.com.ettract.Modal.ChatHistoryModel;
import example.com.ettract.R;
import example.com.ettract.Utility.Sqlite;

/**
 * Created by incipientinfo_pc5 on 12/10/16.
 */
public class ChatFragment extends Fragment {

    Context context;

    ListView listView;

    Sqlite sqlite;

    public static ArrayList<ChatHistoryModel> chatHistoryModelArrayList;

    public static ChatHistoryAdapter chatHistryAdapter;

    ImageView iv_no_data;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_chat, container, false);

        context = container.getContext();

        sqlite = new Sqlite(context);

        String url = "SELECT receiver_id, name, msg FROM tbl_chat GROUP BY receiver_id ORDER BY datetime DESC";

        chatHistoryModelArrayList = sqlite.fetchChat(url);

        listView = (ListView) view.findViewById(R.id.listview_chat);

        iv_no_data = (ImageView) view.findViewById(R.id.iv_no_data);

        if (chatHistoryModelArrayList.size() > 0) {
            chatHistryAdapter = new ChatHistoryAdapter(context, chatHistoryModelArrayList);
        } else {
            iv_no_data.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }


        listView.setAdapter(chatHistryAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("id", chatHistoryModelArrayList.get(i).getReceiver_id());
                intent.putExtra("user_name", chatHistoryModelArrayList.get(i).getName());
                startActivity(intent);

            }
        });

        return view;
    }
}

